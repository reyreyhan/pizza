let pizzaVariant = 0, pizzaSize = 0, pizzaToppings = 0, pizzaPrice = 0

function pizzaVariantValue(pizzaVariantValue) {
    pizzaVariant = pizzaVariantValue
    pizzaTotalPrice();

    if (document.getElementById('pizza1').checked) {
        resetCheckbox()
        document.getElementById("lobster").disabled = true;
        document.getElementById("oyster").disabled = true;
        document.getElementById("salmon").disabled = true;
        document.getElementById("bacon").disabled = true;
        document.getElementById("duck").disabled = true;
        document.getElementById("sausage").disabled = true;
    }

    if (document.getElementById('pizza2').checked) {
        resetCheckbox()
        document.getElementById("avocado").disabled = true;
        document.getElementById("tuna").disabled = true;
        document.getElementById("duck").disabled = true;
        document.getElementById("sausage").disabled = true;
    }

    if (document.getElementById('pizza3').checked) {
        resetCheckbox()
        document.getElementById("avocado").disabled = true;
        document.getElementById("lobster").disabled = true;
        document.getElementById("oyster").disabled = true;
        document.getElementById("salmon").disabled = true;
    }

}

function pizzaSizeValue(pizzaSizeValue) {
    pizzaSize = pizzaSizeValue
    pizzaTotalPrice();
}

function pizzaToppingValue() {
    pizzaToppings = 0
    const toppings = document.getElementsByName('toppings');
    for (let i = 0; i < toppings.length; i++) {
        if (toppings[i].checked) {
            pizzaToppings += parseInt(toppings[i].value)
        }
    }


    console.log(pizzaToppings)
    pizzaTotalPrice();
}

function resetCheckbox() {
    const toppings = document.getElementsByName('toppings');
    for (let i = 0; i < toppings.length; i++) {
        toppings[i].disabled = false;
        toppings[i].checked = false;
    }
    pizzaToppings = 0;
    pizzaTotalPrice();
}

function pizzaTotalPrice() {
    pizzaPrice = parseInt(pizzaVariant) + parseInt(pizzaSize) + parseInt(pizzaToppings)
    document.getElementById("pizza-price").innerHTML = `$${pizzaPrice}`
}